#ifndef __LONG_REAL_NUM__
#define __LONG_REAL_NUM__

#ifndef __MY_STRING__
#include "my_string.h"
#endif // __MY_STRING__

#ifndef __LONG_REAL_NUM_HELPERS__
#include "long_real_num_helpers.h"
#endif // __LONG_REAL_NUM_HELPERS__

class long_real_num {
	private:
	bool _sign;								// true +, false -
	my_string _integer;						// ����� �����
	my_string _fraction;					// ������� �����

	public:
	/* init */
	long_real_num();
	long_real_num(const my_string & str);
	long_real_num(const int in);
	long_real_num(const long_real_num & num);

	/* desctructor */
	~long_real_num();

	/* get & set */
	bool get_sign() const;
	const my_string & get_integer() const;
	const my_string & get_fraction() const;
	void set_sign(bool value);
	void set_integer(const my_string & in);
	void set_fraction(const my_string & fr);

	/* operators */

	// arithmetical
	long_real_num operator- () const;
	long_real_num & operator = (const long_real_num &);

	friend long_real_num operator + (const long_real_num &, const long_real_num &);
	friend long_real_num operator - (const long_real_num &, const long_real_num &);
	friend long_real_num operator * (const long_real_num &, const long_real_num &);
	friend long_real_num operator / (const long_real_num &, const long_real_num &);

	long_real_num & operator += (const long_real_num &);
	long_real_num & operator -= (const long_real_num &);
	long_real_num & operator *= (const long_real_num &);
	long_real_num & operator /= (const long_real_num &);

	// bool
	friend bool operator == (const long_real_num &, const long_real_num &);
	friend bool operator != (const long_real_num &, const long_real_num &);
	friend bool operator > (const long_real_num &, const long_real_num &);
	friend bool operator < (const long_real_num &, const long_real_num &);
	friend bool operator >= (const long_real_num &, const long_real_num &);
	friend bool operator <= (const long_real_num &, const long_real_num &);

	// input & output
	friend std::ostream & operator << (std::ostream & os, const long_real_num & a);
	friend std::istream & operator >> (std::istream & is, long_real_num & a);
};

#endif // __LONG_REAL_NUM__

