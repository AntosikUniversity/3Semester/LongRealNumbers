#ifndef  __MY_STRING__
#define  __MY_STRING__

#include <string.h>
#include <iostream>

int gorn(char * ptr, int number, int osn = 10);

class my_string {
	private:
	char * _data;
	int _length;
	public:
	/* zinit */
	my_string(int num);
	my_string(char * x = 0);
	my_string(const my_string &);

	/* desctructor */
	void clear();
	~my_string();

	/* get & set */
	char * chars() const;									// ��������� ������� ��������
	int length() const;										// ����� �����
	void setlength(int len);								// �������� ����� �����
	my_string insert(int pos, char newchar);					// ��������� ������ �� �������
	my_string insert(int pos, char * chars);					// ��������� ������ �������� �� �������
	my_string substr(int start, int count) const;			// �������� count �������� ������� � ������� start
	int find(char ch) const;								// �������� ������� ������� � �����

	/* operators */

	// arithmetical
	my_string operator += (const my_string & second);
	my_string operator + (const my_string & second);

	// bool
	my_string & operator = (const my_string & second);
	friend my_string operator + (const my_string &, const my_string &);
	int operator == (const my_string & second);
	int operator != (const my_string & second);

	// input & output
	std::istream & read(std::istream & stream);						// ������ �� �������
	std::ostream & write(std::ostream & stream) const;
	my_string readLine(std::istream & stream);						// ������ �� ����� ������
	friend std::ostream & operator << (std::ostream & stream, const my_string & el);
	friend std::istream & operator >> (std::istream & stream, my_string & el);
};

#endif //  __MY_STRING__