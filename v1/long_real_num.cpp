﻿#ifndef __LONG_REAL_NUM__
#include "long_real_num.h"
#endif // __LONG_REAL_NUM__

/* init */
long_real_num::long_real_num() {
	*this = 0;
}

long_real_num::long_real_num(const my_string & string) {
	short int margin = 0, pos = 0;
	pos = string.find('.');
	if (string.chars()[0] == '-') {
		margin = 1;
		_sign = false;
	} else {
		_sign = true;
	}
	if (pos != 0) {
		_integer = string.substr(margin, pos);
		_fraction = string.substr(pos + 1, string.length() - pos - 1);
	} else {
		_integer = string;
		_fraction = "";
	}
}

long_real_num::long_real_num(const int num) {
	_fraction = "";
	_integer = num;
	_sign = (num < 0) ? false : true;
}

long_real_num::long_real_num(const long_real_num & num) {
	_sign = num._sign;
	_integer = num._integer;
	_fraction = num._fraction;
}



/* desctructor */
long_real_num::~long_real_num() {
}



/* get & set */
bool long_real_num::get_sign() const {
	return _sign;
}

const my_string & long_real_num::get_integer() const {
	return _integer;
}

const my_string & long_real_num::get_fraction() const {
	return _fraction;
}

void long_real_num::set_sign(bool value) {
	_sign = value;
}

void long_real_num::set_integer(const my_string & in) {
	_integer = in;

}

void long_real_num::set_fraction(const my_string & fr) {
	_fraction = fr;
}



/* operators */

// arithmetical
long_real_num long_real_num::operator-() const {
	long_real_num res;

	res = *this;
	res.set_sign(!_sign);

	return res;
}


long_real_num operator+(const long_real_num & left, const long_real_num & right) {
	long_real_num result;

	if (left._sign == right._sign) {
		bool add;
		result._sign = left._sign;

		if (left._fraction.length() > right._fraction.length())
			result._fraction = sum_fr(left._fraction, right._fraction, add);
		else
			result._fraction = sum_fr(right._fraction, left._fraction, add);

		if (left._integer.length() > right._integer.length())
			result._integer = sum_int(left._integer, right._integer);
		else
			result._integer = sum_int(right._integer, left._integer);

		if (add) result._integer = sum_int(result._integer, 1);

	} else {

		if (left == -right)
			return 0;

		bool dec = false;

		if (left._sign) {
			if (left > -right) {
				result._sign = true;
				result._integer = diff_int(left._integer, right._integer);
				result._fraction = diff_fr(left._fraction, right._fraction, dec);
			} else {
				result._sign = false;
				result._integer = diff_int(right._integer, left._integer);
				result._fraction = diff_fr(right._fraction, left._fraction, dec);
			}
		} else {
			if (left < -right) {
				result._sign = false;
				result._integer = diff_int(left._integer, right._integer);
				result._fraction = diff_fr(left._fraction, right._fraction, dec);
			} else {
				result._sign = true;
				result._integer = diff_int(right._integer, left._integer);
				result._fraction = diff_fr(right._fraction, left._fraction, dec);
			}
		}
		if (dec) result._integer = sum_int(result._integer, 1);
	}

	return result;
}

long_real_num operator-(const long_real_num & left, const long_real_num & right) {
	return left + -right;
}

/*
	Используем предыдущую версию умножения.
	Сначала просто умножаем, а затем ставим точку в нужное место - profit!
*/
long_real_num operator*(const long_real_num & left, const long_real_num & right) {
	long_real_num result;
	my_string lstring, rstring, multistring;

	if (left == 0 || right == 0)
		return 0;
	if (left == 1)
		return right;
	if (left == -1)
		return -right;
	if (right == 1)
		return left;
	if (right == -1)
		return -left;

	result._sign = left._sign && right._sign;

	lstring = left.get_integer() + left.get_fraction();
	rstring = right.get_integer() + right.get_fraction();
	if (lstring.length() > rstring.length()) {
		multistring = multi(lstring, rstring);
	} else {
		multistring = multi(rstring, lstring);
	}

	size_t point = left.get_fraction().length() + right.get_fraction().length();

	if (point > 0) {
		result.set_integer(multistring.substr(0, point));
		result.set_fraction(multistring.substr(point, multistring.length() - point - 1));
	} else {
		result.set_integer(multistring);
		result.set_fraction("");
	}
	return result;
}

long_real_num operator/(const long_real_num & left, const long_real_num & right) {
	long_real_num result, a, b;
	my_string left_string, right_string;
	my_string result_integer = "", result_fraction = "";
	int value = 0;

	if (right == 0) {
		std::cout << "ERROR! Division Zero" << std::endl;
		return 0;
	}
	if (left == right)
		return 1;
	if (left == -right)
		return -1;
	if (right == 1)
		return left;
	if (right == -1)
		return -left;
	if (left == 0)
		return 0;
	if (left == 1)
		return right;
	if (left == -1)
		return -right;

	result._sign = left._sign && right._sign;

	left_string = left.get_integer() + left.get_fraction();
	right_string = right.get_integer() + right.get_fraction();

	a = left_string;
	b = right_string;

	int left_int_length = left.get_integer().length();
	int right_int_length = right.get_integer().length();
	int left_fr_length = left.get_fraction().length();
	int right_fr_length = right.get_fraction().length();



	/*
		1 проверка: Если у нас длина первого числа больше чем первая цифра второго, то мы просто домножаем второе число на разность разрядов
					И наоборот
	*/
	if (left_int_length > right_int_length) {
		for (int i = 0; i < left_int_length - right_int_length; i++)
			if (right.get_integer().chars()[0] == '0' && right.get_integer().length() == 1) b.set_integer("0" + b.get_integer());
			else b *= 10;
	} else if (left_int_length < right_int_length) {
		for (int i = 0; i < right_int_length - left_int_length; i++)
			if (left.get_integer().chars()[0] == '0' && left.get_integer().length() == 1) a.set_integer("0" + a.get_integer());
			else a *= 10;
	}


	/*
		2 проверка:	Если у нас первая цифра первого числа равна первой цифре второго, то мы просто домножаем второе число, 
							пока оно не будет по длине как первое
					Если же при этом первое число меньше второго, то т.к.это деление нацело(div), мы уменьшаем количество разрядов второго числа на 1
					После этого так же домножаем второе число, пока оно не будет по длине как первое
	*/

	value = a.get_integer().length() - b.get_integer().length();

	if (a.get_integer().chars()[0] > b.get_integer().chars()[0]) {

		for (int i = 0; i < value; i++) b *= 10;
			
	} else if (a.get_integer().chars()[0] == b.get_integer().chars()[0]) {

		for (int i = 0; i < value; i++) b *= 10;

		if (a < b) {
			b.set_integer(b.get_integer().substr(0, b.get_integer().length() - 1));
			value--;
		}

	} else if (a.get_integer().chars()[0] < b.get_integer().chars()[0]) {

		if (value > 0) value--;
		for (int i = 0; i < value; i++) b *= 10;

	}

	value++;
	b *= 10;

	// убираем лишние нули спереди чисел
	while (a.get_integer().length() > 1) {
		if (a.get_integer().chars()[0] == '0') { a.set_integer(a.get_integer().substr(1, a.get_integer().length() - 1)); value--; } else break;
	}
	while (b.get_integer().length() > 1) {
		if (b.get_integer().chars()[0] == '0') { b.set_integer(b.get_integer().substr(1, b.get_integer().length() - 1)); value--; } else break;
	}

	// Integer calc
	/*
		Цикл while: Пока у нас первое число и количество разрядов больше 0, мы 
					1) минусуем из первого числа второе и получаем результат
					2) Пишем в последний разряд целой части результат (1)
	*/
	while (a > 0 && value > 0) {
		b.set_integer(b.get_integer().substr(0, b.get_integer().length() - 1));
		short int x = 0;
		while (a >= b) {
			a -= b;
			x++;
		}

		result_integer.insert(result_integer.length(), (x + 48));
		value--;
	}

	// проверка, вдруг в целую часть ничего не добавили → = 0
	if (result_integer == "") result_integer = "0";


	// Fraction calc
	/*
		Цикл for:	Суммируем количество разрядов в дробной части и после этого в цикле
					1) домножаем первое число на 10 (т.к. мы из него вычитали)
					1) минусуем из первого числа второе и получаем результат
					3) Пишем в последний разряд дробной части результат (2)
	*/
	for (int i = 0; i < left_fr_length + right_fr_length; i++) {
		a *= 10;
		short int x = 0;

		while (a >= b) {
			a -= b;
			x++;
		}

		result_fraction.insert(result_fraction.length(), (x + 48));
	}

	// убираем лишние нули спереди целого числа
	while (result_integer.length() > 1) {
		if (result_integer.chars()[0] == '0') result_integer = result_integer.substr(1, result_integer.length() - 1);
		else break;
	}

	result.set_integer(result_integer);
	result.set_fraction(result_fraction);
	return result;
}

long_real_num & long_real_num::operator=(const long_real_num & left) {
	this->_fraction = left._fraction;
	this->_integer = left._integer;
	this->_sign = left._sign;
	return *this;
}

long_real_num & long_real_num::operator+=(const long_real_num & left) {
	*this = *this + left;

	return *this;
}

long_real_num & long_real_num::operator-=(const long_real_num & left) {
	*this = *this - left;

	return *this;
}

long_real_num & long_real_num::operator*=(const long_real_num & left) {
	*this = *this * left;

	return *this;
}

long_real_num & long_real_num::operator/=(const long_real_num & left) {
	*this = *this / left;

	return *this;
}

long_real_num abs(const long_real_num & left) {
	long_real_num res;

	res.set_sign(true);
	res.set_integer(left.get_integer());
	res.set_fraction(left.get_fraction());

	return res;
}


// bool
bool operator==(const long_real_num & left, const long_real_num & right) {
	if (left._sign != right._sign)
		return false;

	if (left._integer.length() != right._integer.length())
		return false;

	size_t size = right._integer.length();
	for (int i = 0; i < size; i++)
		if (left._integer.chars()[i] != right._integer.chars()[i])
			return false;

	size_t min, max;
	if (left._fraction.length() < right._fraction.length()) {
		min = left._fraction.length();
		max = right._fraction.length();
	} else {
		min = right._fraction.length();
		max = left._fraction.length();
	}

	for (int i = 0; i < min; i++)
		if (left._fraction.chars()[i] != right._fraction.chars()[i])
			return false;

	return false;
}

bool operator!=(const long_real_num & left, const long_real_num & right) {
	return !(left == right);
}

bool operator>(const long_real_num & left, const long_real_num & right) {
	if (left._sign && !right._sign)
		return true;
	if (!left._sign && right._sign)
		return false;

	if (left._integer.length() > right._integer.length()) {
		if (left._sign)
			return true;
		else
			return false;
	}

	if (left._integer.length() < right._integer.length()) {
		if (left._sign)
			return false;
		else
			return true;
	}

	size_t size = left._integer.length();
	for (int i = 0; i < size; ++i)
		if (left._integer.chars()[i] != right._integer.chars()[i]) {
			if (left._integer.chars()[i] > right._integer.chars()[i])
				if (left._sign)
					return true;
				else
					return false;
			else
				if (left._sign)
					return false;
				else
					return true;
		}

	size_t min, max;
	if (left._fraction.length() < right._fraction.length()) {
		min = left._fraction.length();
		max = right._fraction.length();
	} else {
		min = right._fraction.length();
		max = left._fraction.length();
	}

	for (int i = 0; i < min; i++)
		if (left._fraction.chars()[i] != right._fraction.chars()[i]) {
			if (left._fraction.chars()[i] > right._fraction.chars()[i])
				if (left._sign)
					return true;
				else
					return false;
			else
				if (left._sign)
					return false;
				else
					return true;
		}

	return false;
}

bool operator<(const long_real_num & left, const long_real_num & right) {
	return !((left > right) || (left == right));
}

bool operator>=(const long_real_num & left, const long_real_num & right) {
	return ((left > right) || (left == right));
}

bool operator<=(const long_real_num & left, const long_real_num & right) {
	return !(left > right);
}


// input & output
std::ostream & operator<<(std::ostream & os, const long_real_num & num) {
	if (!num._sign) os << '-';

	
	os << num._integer;
	if (num._fraction.length() != 0) os << '.' << num._fraction;

	return os;
}

std::istream & operator >> (std::istream & is, long_real_num & num) {
	my_string str;

	is >> str;
	num = str;

	return is;
}
