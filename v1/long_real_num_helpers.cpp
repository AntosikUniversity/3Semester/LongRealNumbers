#ifndef __LONG_REAL_NUM_HELPERS__
#include "long_real_num_helpers.h"
#endif // __LONG_REAL_NUM_HELPERS__

// Copy from LongNum
/*
	a - ����� ����� ������� ��������, b - �������
	������� ���������� my_string
	a ������ ������ b
	����������:
	size_t asize, bzise - ����� ����� �����
	size_t as, bs - �����-1
	short int value - ����� ���� ���� �����
	bool add - ����� �� ��������� � ��������� ������ +1
	�������� ������:
	� ������ ����� �� ������� �� �������� ������� (����� b) � ���������� �����.
	���� ����� ������ 9 - ���������� +1 � ��������� �������
	�� ������ ����� �� ������������ ����� �������� �����, �.�. ����� ����������, ��� ����� ������ �������� (999 + 1)
	���� ������� ����������� ����� �� ������� - ����������� ��� � ������� setlength
*/
my_string sum_int(const my_string & a, const my_string & b) {
	my_string result = a;
	size_t bsize = b.length(), asize = a.length();
	size_t bs = bsize - 1, i, as = asize - 1;
	short int value;
	bool add = false;
	for (i = 0; i < bsize; i++) {
		value = a.chars()[as - i] + b.chars()[bs - i] - 96;
		if (add) {
			value++;
			add = false;
		}
		if (value > 9) {
			value %= 10;
			add = true;
		}
		result.chars()[as - i] = value + 48;
	}

	while (add && i < asize) {
		value = a.chars()[as - i] - 47;
		add = false;
		if (value > 9) {
			value %= 10;
			add = true;
		}
		result.chars()[as - i] = value + 48;
		i++;
	}

	if (add) {
		result.insert(0,'1');
	}

	return result;
};


/*
	a - ������� ����� ������� ��������, b - �������, ad - bool, ���� �� �������, ����� �� ����������� ����� ����� �� 1
	������� ���������� my_string
	a ������ ������ b
	����������:
	size_t asize, bzise - ����� ����� �����
	short int value - ����� ���� ���� �����
	bool add - ����� �� ��������� � ��������� ������ +1
	�������� ������:
	� ������ ����� �� ��������� n ����� � ����� ����������, ��� n - �������� �������� ����� a � b.
	�� ������ ����� �� ������� �� �������� ������� (����� b) � ���������� �����.
	���� ����� ������ 9 - ���������� +1 � ��������� �������
	� ������� ����� �� ������������ ����� �������� �����, �.�. ����� ����������, ��� ����� ������ �������� (999 + 1)
	���� ���� ���������� ����� �����, ���������� � ad ��� ����� �������� 1 � ����� �����
*/
my_string sum_fr(const my_string & a, const my_string & b, bool & ad) {
	ad = false;
	my_string result = a;
	size_t bsize = b.length() - 1, asize = a.length() - 1, i;
	short int value;
	bool add = false;

	if (a.length() == 0) return b;
	if (b.length() == 0) return a;

	if (asize < bsize) {
		for (int i = asize; i < bsize; i++) {
			char ins[2] = { '0', '\0' }, *pIns = ins;
			result.insert(result.length(), pIns);
		}
	}

	for (i = bsize; i > 0; i--) {
		value = a.chars()[i] + b.chars()[i] - 96;
		if (add) {
			value++;
			add = false;
		}
		if (value > 9) {
			value %= 10;
			add = true;
		}
		result.chars()[i] = value + 48;
	}

	value = a.chars()[0] + b.chars()[0] - 96;
	if (add)
		value++;
	if (value > 9) {
		value %= 10;
		ad = true;
	}
	result.chars()[0] = value + 48;

	return result;
};


// Copy from LongNum
/*
	a - ������ �������, b - ������
	������� ���������� my_string
	a ������ ������ b
	����������:
	size_t asize, bzise - ����� ����� �����
	size_t as, bs - �����-1
	short int value - �������� ���� ���� �����
	bool decreases - ����� �� ��������� � ��������� ������ -1
	�������� ������:
	� ������ ����� �� ������� �� �������� ������� (����� b) � �������� ����������� �� ������� ����� ����� �������.
	���� ����� ������ 0 - �������� 1 �� ��������� �������
	�� ������ ����� �� ������������ ����� �������� �����, �.�. ����� ����������, ��� ����� ������ �� ������� �������� (1000 - 1)
	� ������� �� ����������, ���� �� � ��� ���� � �������� (�������). ���� �� - �������.
*/
my_string diff_int(const my_string & a, const my_string & b) {
	my_string result = a;
	size_t bsize = b.length(), asize = a.length();
	size_t bs = bsize - 1, i, as = asize - 1;
	short int value;
	bool decreases = false;
	for (i = 0; i < bsize; i++) {
		if (decreases) {
			decreases = false;
			value = a.chars()[as - i] - b.chars()[bs - i] - 1;
		} else {
			value = a.chars()[as - i] - b.chars()[bs - i];
		}

		if (value < 0) {
			decreases = true;
			value += 10;
		}

		result.chars()[as - i] = value + 48;
	}

	while (decreases && i < asize) {
		value = a.chars()[as - i] - 49;
		decreases = false;
		if (value < 0) {
			value += 10;
			decreases = true;
		}
		result.chars()[as - i] = value + 48;
		i++;
	}

	for (i = 0; i < asize; i++)
		if (result.chars()[i] != '0')
			break;

	if (i == asize)
		result = result.substr(i - 1, result.length() - i + 1);
	else
		result = result.substr(i, result.length() - i);

	return result;
};

/*
	a - ������ �������, b - ������, dec - bool, ���� �� ����������, ����� �� ����� ������� -1 � ����� �����
	������� ���������� my_string
	a ������ ������ b
	����������:
	size_t asize, bzise - ����� ����� �����
	size_t min - ����������� �� ����� �����
	short int value - �������� ���� ���� �����
	bool decreases - ����� �� ��������� � ��������� ������ -1
	�������� ������:
	� ������ ����� �� ���������, ����� �� �������� ����� � ����� ������ �� ����� (����� �������� ���������� ��������)
	�� ������ ����� �� ������� �� �������� ������� (����� b) � �������� ���������� �� ������� ����� ����� �������. (�� ����� �� ������)
	���� ����� ������ 0 - �������� 1 �� ���������� �������

*/
my_string diff_fr(const my_string & a, const my_string & b, bool & dec) {
	dec = false;
	my_string result = a;
	size_t sizeA = a.length() - 1, sizeB = b.length() - 1;
	size_t min = sizeA > sizeB ? sizeB : sizeA;

	
	if (a.length() == 0) return b;
	if (b.length() == 0) return a;

	if (sizeA < sizeB) {
		for (int i = sizeA; i < sizeB; i++) {
			char ins[2] = { '0', '\0' }, *pIns = ins;
			result.insert(result.length(), pIns);
		}
	}

	short int value;
	bool decreases = false;
	for (int i = min; i > 0; i--) {
		value = a.chars()[i] - b.chars()[i];
		if (decreases) {
			value--;
			decreases = false;
		}
		if (value < 0) {
			value += 10;
			decreases = true;
		}
		result.chars()[i] = value + 48;
	}

	value = a.chars()[0] - b.chars()[0];
	if (decreases)
		value--;
	if (value < 0) {
		value += 10;
		dec = true;
	}
	result.chars()[0] = value + 48;

	return result;
};

// Copy from LongNum
/*
	a - ������ �������, b - ������ (�����)
	a ������ ������ b
	������� ���������� MyString
	����������:
	MyString result - ���� ������������ �����
	short int add = 0 - �����, ������� ����� �������� � ��������� ������
	short int value - ����� � �������
	��������:
	� 1 ����� �� ���������� �������� �� a �� ����� b � ��������� ����������.
	������ ����� ����� ��������� ����� ������ �� 10: ������� ���������� � ������ ����������, � ����� ���������� � add
	����� �� ����������� �� �� ����� � ������ ��������.
	���� ��� ��� ��� ����� �������� ���-�� � ������ ����, �� ��������� ��� � ������� ������� insert
*/
my_string multi(const my_string & a, short int b) {
	my_string result = a;
	short int add = 0, value;

	for (size_t i = a.length() - 1; i > 0; i--) {
		value = (a.chars()[i] - 48) * b + add;
		add = 0;
		if (value > 9) {
			add = value / 10;
			value %= 10;
		}
		result.chars()[i] = value + 48;
	}

	value = (a.chars()[0] - 48) * b + add;
	add = 0;
	if (value > 9) {
		add = value / 10;
		value %= 10;
	}
	result.chars()[0] = value + 48;

	if (add != 0) {
		char ins[2] = { add + 48, '\0' }, *pIns = ins;
		result.insert(0, pIns);
	}

	return result;
}

// Copy from LongNum
/*
	a - ������ �������, b - ������
	a ������ ������ b
	������� ���������� MyString
	����������:
	LongNum result - ���� ������������ �����
	MyString _a - �����, ������� �� ������������
	short int value - ����� � �������
	��������:
	� 1 ����� �� ���������� �������� �� _a �� ����� b � ������� ������� 'multi', ������� �������� 0 � ����� ����� _a
	�� ������ ������� �� ���������, �������� �� ��������� ������ �������� ��� ���, ����� ���� ��������
	P.S. �������� �� value == 1 �����, ����� �������������� ������ ������� (����� ������ ������)
*/
my_string multi(const my_string & a, const my_string & b) {
	long_real_num result;
	my_string _a = a;

	for (size_t i = b.length() - 1; i > 0; i--) {
		short int value = b.chars()[i] - 48;

		if (value == 1)
			result += _a;
		else
			if (value > 1)
				result += multi(_a, value);
		char ins[2] = { '0', '\0' }, *pIns = ins;
		_a.insert(_a.length(), pIns);
	}

	short int value = b.chars()[0] - 48;

	if (value == 1)
		result += _a;
	else
		if (value > 1)
			result += multi(_a, value);

	return result.get_integer();
};