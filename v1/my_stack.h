#ifndef __MY_STACK__
#define __MY_STACK__

template<typename stack_type>
class my_stack {
	private:
	stack_type * _el;					// ������� �������
	my_stack * _next;				// ��������� �������

	public:
	/* init */
	my_stack(stack_type elem = 0, my_stack * next = nullptr) {
		_el = new stack_type(elem);
		_next = next;
	};


	/* manipulate */
	bool push(stack_type & elem) {
		if (_el == nullptr) {
			try {
				_next = nullptr;
				_el = new stack_type(elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}

		} else {
			try {
				_next = new my_stack(*_el, _next);
				_el = new stack_type(elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}
		}
		return true;
	};
	bool pushP(stack_type * elem) {
		if (_el == nullptr) {
			try {
				_next = nullptr;
				_el = new stack_type(*elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}

		} else {
			try {
				_next = new my_stack(*_el, _next);
				_el = new stack_type(*elem);
			}
			catch (const std::bad_alloc & e) {
				cout << "ERROR: Memory allocation - " << e.what() << endl;
				return false;
			}
		}
		return true;
	};
	stack_type pop() {
		stack_type frac = *_el;
		my_stack * next = _next;
		if (next) {
			_el = next->_el;
			_next = next->_next;
		} else {
			_el = nullptr;
			_next = nullptr;
		}
		return frac;
	};
	void clear() {
		if (_el) delete _el;
		if (_next) delete _next;
		_el = nullptr;
		_next = nullptr;
	}
	int length() {
		int res = 0;
		my_stack * temp = this;
		while (temp->_next) { res++; temp = temp->_next; }
		return res + 1;
	}
	void print() {
		my_stack * temp = this;
		while (temp->_next) { cout << *(temp->_el) << " , "; temp = temp->_next; }
		return;
	};

	/* get & set */
	const stack_type * get_el() {
		return _el;
	};
	void set_el(stack_type el) {
		_el = &el;
		return;
	}
	my_stack * get_next_el() {
		return _next;
	}


	/* desctructor */
	~my_stack() {
		if (_el) delete _el;
		if (_next) delete _next;
	};
};

#endif // __MY_STACK__
