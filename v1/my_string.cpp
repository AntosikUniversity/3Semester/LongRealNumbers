#ifndef __MY_STRING__
#include "my_string.h"
#endif // __MY_STRING__

int gorn(char *ptr, int number, int osn) {
	int num = number, n = number, NumberOfDigits = 0;

	char *r = ptr;
	while (n) {
		n /= osn; NumberOfDigits++;
	}
	while (NumberOfDigits--) r++;
	*r = 0;
	NumberOfDigits++;
	while (num) {
		*--r = ((num % osn <10) ? (num % osn + '0') : (num % osn + 'A' - 10));
		NumberOfDigits++;
		num /= osn;
	}
	return NumberOfDigits;
}


/*init */
// ����������� �� �����
my_string::my_string(int number) {
	char *temp = new char[256];
	_length = gorn(temp, number);
	_data = new char[_length + 1];
	strcpy(_data, temp);
	delete temp;
}

// ����������� �� ������� char
my_string::my_string(char *x)
{
	if (x) {
		_length = strlen(x);
		_data = new char[_length + 1];
		strcpy(_data, x);
	}
	else {
		_data = NULL;
		_length = 0;
	}
}

// ����������� �����
my_string::my_string(const my_string & x)
{
	if (x._length > 0) {
		_length = x._length;
		_data = new char[_length + 1];
		strcpy(_data, x._data);
	}
	else {
		_length = 0;
		_data = NULL;
	}
}


/*get & set */
// �������� ������ ��������
char *my_string::chars() const
{
	return _data;
}

// �������� ����� �����
int my_string::length() const
{
	return _length;
}

// ������������� ����� ����� 
void my_string::setlength(int len)
{
	char *temp = new char[len + 1];
	for (int i = 0; i < len; i++) {
		temp[i] = '0';
	}
	temp[len] = '\0';
	_length = len;
	strncpy(temp, _data, _length);
	delete[] _data;
	_data = temp;
}

// ������ �����
void my_string::clear() {
	delete[] _data;
	_data = NULL;
	_length = 0;
}

// ��������� ������ "c"  �� ������� "pos"
my_string my_string::insert(int pos, char c) {
	int len = _length + 1;
	char tchar[2];
	tchar[0] = c;
	tchar[1] = '\0';
	char *temp = new char[_length + 1 + 1];
	for (int i = 0; i < len; i++) {
		temp[i] = '0';
	}
	temp[len] = '\0';
	strncpy(temp, _data, pos);
	strncpy(temp + pos, tchar, 1);
	strncpy(temp + pos + 1, _data + pos - 1 + 1, _length - pos);
	_length += 1;
	delete[] _data;
	_data = temp;
	return *this;
}

// ��������� ������ �������� "chars" �� ������� "pos"
my_string my_string::insert(int pos, char *chars) {
	int len = _length + strlen(chars);
	char *temp = new char[_length + strlen(chars) + 1];
	for (int i = 0; i < len; i++) {
		temp[i] = '0';
	}
	temp[len] = '\0';
	strncpy(temp, _data, pos);
	strncpy(temp+pos, chars, strlen(chars));
	strncpy(temp + pos + strlen(chars), _data + pos -1  + strlen(chars), _length - pos);
	_length += strlen(chars);
	delete[] _data;
	_data = temp;
	return *this;
}

// �������� "count" �������� ������� � ������� "start"
my_string my_string::substr(int start, int count) const
{
	my_string newStr;
	if ((start > _length) || (start + count > _length)) {
		return NULL;
	} else {
		newStr._length = count;
		newStr._data = new char[count + 1];
		strncpy(newStr._data, _data + start, count);
		newStr._data[count] = '\0';
	}
	return newStr;
}

// �������� ������� ������� "c"
int my_string::find(char c) const {
	char *p;
	p = strchr(_data, c);
	if (p != 0) return p - _data;
	else return 0;
}


/* desctructor */
my_string::~my_string()
{
	if (_data) delete[] _data;
}


/* operators */

// arithmetical
my_string my_string::operator + (const my_string & second)
{
	my_string c = *this;
	c += second;
	return c;
}

my_string my_string::operator += (const my_string & second)
{
	int len = _length + second._length;
	char *temp = new char[len + 1];
	for (int i = 0; i < len; i++) {
		temp[i] = '0';
	}
	temp[len] = '\0';
	strncpy(temp, _data, _length);
	strncpy(temp + _length, second._data, second._length);
	delete[] _data;
	_data = temp;
	_length = len;
	return *this;
}

// bool
my_string & my_string::operator = (const my_string & second)
{
	if (second._length > 0) {
		_length = second._length;
		char *temp = new char[_length + 1];
		strcpy(temp, second._data);
		if (_data)
			delete[] _data;
		_data = temp;
	}
	else {
		_length = 0;
		if (_data)
			delete[] _data;
		_data = NULL;
	}
	return *this;
}

int my_string::operator == (const my_string & second)
{
	return !strcmp(_data, second._data);
}

int my_string::operator != (const my_string & second)
{
	return strcmp(_data, second._data);
}

// input & output
my_string my_string::readLine(std::istream & stream)
{
	long int eoln;
	char inval[255];
	char *ptr;
	do {
		stream.getline(inval, 255);
		ptr = strchr(inval, '\0');
		eoln = ptr - inval;
	} while (!eoln && !stream.eof());

	if (_data) delete[] _data;
	_length = strlen(inval);
	_data = new char[_length + 1];
	strcpy(_data, inval);
	return *this;
}

std::istream & my_string::read(std::istream & stream)
{
	char inval[255];
	inval[0] = '\0';
	char c;
	stream >> std::noskipws;

	do {
		stream >> c;
		if (c != ' ' && c != '\n')
		{
			char temp[2];
			temp[0] = c;
			temp[1] = '\0';
			strcat(inval, temp);
		}
	} while (c != ' ' && c != '\n');

	_length = strlen(inval);
	_data = new char[_length + 1];
	strcpy(_data, inval);
	return stream;

}

std::ostream & my_string::write(std::ostream & stream) const
{
	stream << _data;
	return stream;
}


my_string operator+(const my_string & left, const my_string & right) {
	my_string res;
	res += left;
	res += right;
	return res;
}

std::ostream & operator << (std::ostream & stream, const my_string & el)
{
	return el.write(stream);
}

std::istream & operator >> (std::istream & stream, my_string & el)
{
	return el.read(stream);
}

