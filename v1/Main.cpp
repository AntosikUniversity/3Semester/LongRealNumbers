#include "long_real_num.h"
#include "my_stack.h"
#include "my_string.h"
#include <iostream>
#include <fstream>

using namespace std;

const char* file_input_name = "input.txt";
int priority(const char oper);

int main() {
	ifstream file_input;
	file_input.open(file_input_name, ios::in);

	char c = ' ', _c = ' ';
	char nowChar = ' ', topChar = ' ';
	my_string number;

	long_real_num Prev(0), Now(0), Result(0);
	my_stack<char> operators('(');
	my_stack<long_real_num> long_real_nums(long_real_num(0));

	while (!file_input.eof()) {
		file_input >> c;
		if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%' || c == '(' || c == ')' || c == ';') {
			if (isdigit(_c)) {
				long_real_nums.pushP(new long_real_num(number));
				number.clear();
			}
			if (c == '(') {
				operators.push(c);
			} else if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%') {
				if (priority(c) <= priority(*operators.get_el())) {
					Now = long_real_nums.pop(); Prev = long_real_nums.pop();
					switch (nowChar = operators.pop()) {
						case '+': Result = Prev + Now; cout << '\n' << Prev << " + " << Now << " = " << Result; break;
						case '-': Result = Prev - Now; cout << '\n' << Prev << " - " << Now << " = " << Result; break;
						case '*': Result = Prev * Now; cout << '\n' << Prev << " * " << Now << " = " << Result; break;
						case '/': Result = Prev / Now; cout << '\n' << Prev << " / " << Now << " = " << Result; break;
						default: printf("Undefined!"); break;
					}
					long_real_nums.pushP(&Result);
				}
				operators.push(c);
			} else if (c == ')' || c == ';') {
				do {
					nowChar = operators.pop();
					int asd = operators.length();
					if (nowChar == '(' || nowChar == ' ') break;
					Now = long_real_nums.pop(); Prev = long_real_nums.pop();
					switch (nowChar) {
						case '+': Result = Prev + Now; cout << '\n' << Prev << " + " << Now << " = " << Result; break;
						case '-': Result = Prev - Now; cout << '\n' << Prev << " - " << Now << " = " << Result; break;
						case '*': Result = Prev * Now; cout << '\n' << Prev << " * " << Now << " = " << Result; break;
						case '/': Result = Prev / Now; cout << '\n' << Prev << " / " << Now << " = " << Result; break;
						default: printf("Undefined!"); break;
					}
					long_real_nums.pushP(&Result);
				} while (nowChar != '(' && nowChar != ' ');
				if (nowChar == '(' && c == ';') {
					char cc = '(';
					operators.clear(); long_real_nums.clear();
					operators.push(cc);
					long_real_nums.pushP(new long_real_num(0));
					Prev = 0; Now = 0; Result = 0;
					cout << "\n\n-----------------------------------\n";
				}
			}
		} else if (isdigit(c) || c == '.') {
			number.insert(number.length(), c);
		};
		_c = c;
	}

	file_input.close();
	system("pause");
	return 0;
}

int priority(const char oper) { // return priority 1 - +-; 2 - */; 3 - (
	switch (oper) {
		case '(': case ')': return 0;
		case '+': case'-': return 1;
		case '*': case'/': case'%': return 2;
		default:
			break;
	}
}