#ifndef __LONG_REAL_NUM_HELPERS__
#define __LONG_REAL_NUM_HELPERS__

#ifndef __MY_STRING__
#include "my_string.h"
#endif // __MY_STRING__

#ifndef __LONG_REAL_NUM__
#include "long_real_num.h"
#endif // __LONG_REAL_NUM__

my_string sum_int(const my_string & a, const my_string & b);				// from LongNum
my_string sum_fr(const my_string & a, const my_string & b, bool & add);

my_string diff_int(const my_string & a, const my_string & b);				// from LongNum
my_string diff_fr(const my_string & a, const my_string & b, bool & dec);

my_string multi(const my_string & a, short int b);							// from LongNum
my_string multi(const my_string & a, const my_string & b);					// from LongNum

#endif // __LONG_REAL_NUM_HELPERS__
